/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package hospital;

import java.util.List;

/**
 *
 * @author DHAIRYA
 */
public class ResarchGroup {
    private String wait_time;
    private List<Patients> patients;

    public ResarchGroup(String wait_time, List<Patients> patients) {
        this.wait_time = wait_time;
        this.patients = patients;
    }

    public String getWait_time() {
        return wait_time;
    }

    public void setWait_time(String wait_time) {
        this.wait_time = wait_time;
    }

    public List<Patients> getPatients() {
        return patients;
    }

    public void setPatients(List<Patients> patients) {
        this.patients = patients;
    }
    
    
}

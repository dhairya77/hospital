/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package hospital;

import java.util.List;

/**
 *
 * @author DHAIRYA
 */
public class Wrist_Band {
    private String bar_code;
    private List<Patients> patients;

    public Wrist_Band(String bar_code, List<Patients> patients) {
        this.bar_code = bar_code;
        this.patients = patients;
    }

    public String getBar_code() {
        return bar_code;
    }

    public void setBar_code(String bar_code) {
        this.bar_code = bar_code;
    }

    public List<Patients> getPatients() {
        return patients;
    }

    public void setPatients(List<Patients> patients) {
        this.patients = patients;
    }
    
    
            
    
}

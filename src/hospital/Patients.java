/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package hospital;

/**
 *
 * @author DHAIRYA
 */
public class Patients {
    private String name;
    private String dob;
    private String family_doc;

    public Patients(String name, String dob, String family_doc) {
        this.name = name;
        this.dob = dob;
        this.family_doc = family_doc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getFamily_doc() {
        return family_doc;
    }

    public void setFamily_doc(String family_doc) {
        this.family_doc = family_doc;
    }
    
    
}
